<?php
include_once 'add/Config.php';

class RegistraBoleto
{

    const TICKET_ENDPOINT = "https://ymbdlb.santander.com.br/dl-ticket-services/TicketEndpointService/TicketEndpointService.wsdl";
    const COBRANCA_ENDPOINT = "https://ymbcash.santander.com.br/ymbsrv/CobrancaEndpointService/CobrancaEndpointService.wsdl";

    function __construct()
    {
        
    }

    function getTicketXmlRegistro($dados)
    {
        $parametros = [
            ['key' => 'CONVENIO.COD-BANCO', 'value' => '0033'],
            ['key' => 'CONVENIO.COD-CONVENIO', 'value' => '6626254'],
            ['key' => 'PAGADOR.TP-DOC', 'value' => $dados['PAGADOR.TP-DOC']],
            ['key' => 'PAGADOR.NUM-DOC', 'value' => $dados['PAGADOR.NUM-DOC']],
            ['key' => 'PAGADOR.NOME', 'value' => $dados['PAGADOR.NOME']],
            ['key' => 'PAGADOR.ENDER', 'value' => $dados['PAGADOR.ENDER']],
            ['key' => 'PAGADOR.BAIRRO', 'value' => $dados['PAGADOR.BAIRRO']],
            ['key' => 'PAGADOR.CIDADE', 'value' => $dados['PAGADOR.CIDADE']],
            ['key' => 'PAGADOR.UF', 'value' => $dados['PAGADOR.UF']],
            ['key' => 'PAGADOR.CEP', 'value' => $dados['PAGADOR.CEP']],
            ['key' => 'TITULO.NOSSO-NUMERO', 'value' => $dados['TITULO.SEU-NUMERO']],
            ['key' => 'TITULO.SEU-NUMERO', 'value' => $dados['TITULO.SEU-NUMERO']],
            ['key' => 'TITULO.DT-VENCTO', 'value' => $dados['TITULO.DT-VENCTO']],
            ['key' => 'TITULO.DT-EMISSAO', 'value' => $dados['TITULO.DT-EMISSAO']],
            ['key' => 'TITULO.ESPECIE', 'value' => $dados['TITULO.ESPECIE']],
            ['key' => 'TITULO.VL-NOMINAL', 'value' => $dados['TITULO.VL-NOMINAL']],
            ['key' => 'TITULO.PC-MULTA', 'value' => $dados['TITULO.PC-MULTA']],
            ['key' => 'TITULO.QT-DIAS-MULTA', 'value' => $dados['TITULO.QT-DIAS-MULTA']],
            ['key' => 'TITULO.PC-JURO', 'value' => $dados['TITULO.PC-JURO']],
            ['key' => 'TITULO.TP-DESC', 'value' => $dados['TITULO.TP-DESC']],
            ['key' => 'TITULO.VL-DESC', 'value' => $dados['TITULO.VL-DESC']],
            ['key' => 'TITULO.DT-LIMI-DESC', 'value' => $dados['TITULO.DT-LIMI-DESC']],
            ['key' => 'TITULO.VL-ABATIMENTO', 'value' => $dados['TITULO.VL-ABATIMENTO']],
            ['key' => 'TITULO.TP-PROTESTO', 'value' => $dados['TITULO.TP-PROTESTO']],
            ['key' => 'TITULO.QT-DIAS-PROTESTO', 'value' => $dados['TITULO.QT-DIAS-PROTESTO']],
            ['key' => 'TITULO.QT-DIAS-BAIXA', 'value' => $dados['TITULO.QT-DIAS-BAIXA']],
            ['key' => 'MENSAGEM', 'value' => $dados['MENSAGEM']]
        ];

        $ticketRequest = array('dados' => $parametros,
            'expiracao' => 100,
            'sistema' => 'YMB');

        $toReturn = array('TicketRequest' => $ticketRequest);

        return $toReturn;
    }

    function getTicketXmlConsulta()
    {
        $parametros = [
            ['key' => 'CONVENIO.COD-BANCO', 'value' => '0033'],
            ['key' => 'CONVENIO.COD-CONVENIO', 'value' => '6626254']
        ];

        $ticketRequest = array(
            'dados' => $parametros,
            'expiracao' => 100,
            'sistema' => 'YMB'
        );

        $toReturn = array('TicketRequest' => $ticketRequest);

        return $toReturn;
    }

    function getRegistroXml($ticket, $num)
    {
        $dtNsu = date("dmY");

        $inclusaoTitulo = array(
            'dtNsu' => $dtNsu,
            'estacao' => 'IHZU',
            'nsu' => $num,
            'ticket' => $ticket,
            'tpAmbiente' => 'P'
        );

        $toReturn = array('dto' => $inclusaoTitulo);

        return $toReturn;
    }

    function getConsultaXml($ticket, $num, $dtNsu)
    {
        $inclusaoTitulo = array(
            'dtNsu' => $dtNsu,
            'estacao' => 'IHZU',
            'nsu' => $num,
            'ticket' => $ticket,
            'tpAmbiente' => 'P'
        );

        $toReturn = array('dto' => $inclusaoTitulo);

        return $toReturn;
    }

    function registrarBoleto($dados, $pasta)
    {
        try {

            $options = array(
                'keep_alive' => false,
                'trace' => true,
                'local_cert' => $pasta . '/santander/dantas.pem',
                'passphrase' => '123Hostdime@',
                'cache_ws' => WSDL_CACHE_NONE
            );

            //gerar Ticket
            $cliTicket = new SoapClient(self::TICKET_ENDPOINT, $options);

            //gerar consulta ou registro
            $cliCobranca = new SoapClient(self::COBRANCA_ENDPOINT, $options);

            $xmlCreateReg = self::getTicketXmlRegistro($dados);
            $cTicketRegistro = $cliTicket->create($xmlCreateReg);

            $xmlRegistro = self::getRegistroXml($cTicketRegistro->TicketResponse->ticket, $dados['TITULO.SEU-NUMERO']);
            $rRegistro = $cliCobranca->registraTitulo($xmlRegistro);

            if (intval($rRegistro->return->situacao) == 20) {

                $nsuReturn = $rRegistro->return->nsu;
                $dtNsuReturn = $rRegistro->return->dtNsu;

                $xmlCreateCons = self::getTicketXmlConsulta();
                $cTicketConsulta = $cliTicket->create($xmlCreateCons);

                $xmlConsulta = self::getConsultaXml($cTicketConsulta->TicketResponse->ticket, $nsuReturn, $dtNsuReturn);
                $rConsulta = $cliCobranca->consultaTitulo($xmlConsulta);

                return $rConsulta;
            } else {
                return $rRegistro;
            }
        } catch (SoapFault $e) {
            return var_dump($e);
        }
    }
}

?>