<?php

class Config
{

    private static $caminhoArquivoConfig = __DIR__ . "/config.ini";
    private static $config = array(
        "convenio" => array(
            "banco_padrao" => "0033",
            "convenio_padrao" => ""),
        "instrucoes_banco" => array(
            "tipo_desconto" => 0,
            "valor_desconto" => 0,
            "data_limite_desconto" => "now",
            "tipo_protesto" => 0,
            "baixar_apos" => 0),
        "geral" => array(
            "assegurar_endpoint" => true,
            "timeout" => 30,
            "ambiente" => "P",
            "formato_data" => "dmY",
            "estacao" => ""),
        "certificado" => array(
            "arquivo" => "",
            "senha" => "",
            "tipo_arquivo" => "PEM",
            "diretorio_cas" => "",
            "arquivo_ca" => "")
    );
    private static $instance;

    public function __construct()
    {
        $this->carregarConfiguracao();
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new Config();
        }
        return self::$instance;
    }

    public static function getCaminhoArquivoConfig()
    {
        return self::$caminhoArquivoConfig;
    }

    public static function setCaminhoArquivoConfig($caminhoArquivoConfig)
    {
        $caminhoAnterior = self::$caminhoArquivoConfig;
        self::$caminhoArquivoConfig = $caminhoArquivoConfig;

        if (!is_null(self::$instance) && $caminhoAnterior != $caminhoArquivoConfig) {
            self::carregarConfiguracao();
        }
    }

    public function getConvenio($chave)
    {
        return $this->getOpcao("convenio", $chave);
    }

    public function getInstrucao($chave)
    {
        return $this->getOpcao("instrucoes_banco", $chave);
    }

    public function getGeral($chave)
    {
        return $this->getOpcao("geral", $chave);
    }

    public function getCertificado($chave)
    {
        return $this->getOpcao("certificado", $chave);
    }

    public function getOpcao($grupo, $chave)
    {
        if (isset(self::$config[$grupo][$chave])) {
            return self::$config[$grupo][$chave];
        } else {
            throw new \InvalidArgumentException("O valor da chave " . $chave . " pertencente ao grupo " . $grupo . " não existe.");
        }
    }

    private static function carregarConfiguracao()
    {
        if (file_exists(self::$caminhoArquivoConfig)) {
            self::$config = parse_ini_file(self::$caminhoArquivoConfig, true);
        } else {
            self::criarArquivoDeConfiguracao();
        }
    }

    private static function criarArquivoDeConfiguracao()
    {
        $handler = @fopen(self::$caminhoArquivoConfig, "w");
        if ($handler) {
            $ini = self::converterArrayDeConfiguracaoParaINI();

            fwrite($handler, $ini);
            fclose($handler);
        } else {
            $err = error_get_last();
            throw new \Exception("Não foi possível encontrar o arquivo de configuração e, ao tentar criá-lo, ocorreu o seguinte erro: " . $err["message"]);
        }
    }

    private static function converterArrayDeConfiguracaoParaINI()
    {
        $ini = "; Este arquivo foi criado automaticamente ao tentar acessar as configurações do WS Boleto Santander
                ; Arquivo de configuração gerado em " . date("d/m/Y H:i:s") . PHP_EOL;

        foreach (self::$config as $grupo => $chaves) {
            $ini .= PHP_EOL . self::converterStringEmGrupoDeConfiguracaoINI($grupo);
            foreach ($chaves as $chave => $valor) {
                $ini .= self::converterChaveEValorEmLinhaDeOpcaoINI($chave, $valor);
            }
        }

        return $ini;
    }

    private static function converterStringEmGrupoDeConfiguracaoINI($string)
    {
        return "[" . $string . "]" . PHP_EOL;
    }

    private static function converterChaveEValorEmLinhaDeOpcaoINI($chave, $valor)
    {
        $str = $chave . " = ";
        if (is_string($valor)) {
            $valor = '"' . $valor . '"';
        }
        $str .= $valor . PHP_EOL;
        return $str;
    }
}
