<?php
/**
 * WooCommerce Boleto Template.
 */
// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

@ob_start();

global $wp_query;

// Support for plugin older versions.
$boleto_code = isset($_GET['ref']) ? $_GET['ref'] : $wp_query->query_vars['boleto'];

// Test if exist ref.
if (isset($boleto_code)) {

    // Sanitize the ref.
    $ref = sanitize_title($boleto_code);

    // Gets Order id.
    $order_id = woocommerce_get_order_id_by_order_key($ref);

    if ($order_id) {
        // Gets the data saved from boleto.
        $order = new WC_Order($order_id);
        $order_data = get_post_meta($order_id, 'wc_boleto_data', true);

        // Gets current bank.
        $settings = get_option('woocommerce_boleto_settings');
        $bank = sanitize_text_field($settings['bank']);

        if ($bank) {

            // Sets the boleto details.
            $logo = sanitize_text_field($settings['boleto_logo']);
            $shop_name = get_bloginfo('name');

            // Sets the boleto data.
            $data = array();
            foreach ($order_data as $key => $value) {
                $data[$key] = sanitize_text_field($value);
            }

            // Sets the settings data.
            foreach ($settings as $key => $value) {
                if (in_array($key, array('demonstrativo1', 'demonstrativo2', 'demonstrativo3'))) {
                    $data[$key] = str_replace('[number]', '#' . $data['nosso_numero'], sanitize_text_field($value));
                } else {
                    $data[$key] = sanitize_text_field($value);
                }
            }

            // Set the ticket total.
            $data['valor_boleto'] = number_format((float) $order->get_total(), 2, ',', '');

            // Shop data.
            $data['identificacao'] = $shop_name;

            // Client data.
            /* if (!empty($order->billing_cnpj)) {
              $data['sacado'] = $order->billing_company;
              } else { */
            $data['sacado'] = $order->billing_first_name . ' ' . $order->billing_last_name;
            //}
            // Formatted Addresses
            $address_fields = apply_filters('woocommerce_order_formatted_billing_address', array(
                'first_name' => '',
                'last_name' => '',
                'company' => '',
                'address' => $order->billing_address_1,
                'bairro' => $order->billing_bairro,
                'city' => $order->billing_city,
                'state' => $order->billing_address_2,
                'postcode' => $order->billing_cep_2,
                'country' => $order->billing_country
                ), $order);

            if (defined('WC_VERSION') && version_compare(WC_VERSION, '2.1', '>=')) {
                $address = WC()->countries->get_formatted_address($address_fields);
            } else {
                global $woocommerce;
                $address = $woocommerce->countries->get_formatted_address($address_fields);
            }

            // Get Extra Checkout Fields for Brazil options.
            /* $wcbcf_settings = get_option('wcbcf_settings');
              $customer_document = '';
              if (0 != $wcbcf_settings['person_type']) {
              if (( 1 == $wcbcf_settings['person_type'] && 1 == $order->billing_persontype ) || 2 == $wcbcf_settings['person_type']) {
              $customer_document = __('CPF:', 'woocommerce-boleto-registrado-santander') . ' ' . $order->billing_cpf;
              }

              if (( 1 == $wcbcf_settings['person_type'] && 2 == $order->billing_persontype ) || 3 == $wcbcf_settings['person_type']) {
              $customer_document = __('CNPJ:', 'woocommerce-boleto-registrado-santander') . ' ' . $order->billing_cnpj;
              }
              } */

            // Set the customer data.
            $end = explode(',', $order->billing_address_1);

            if (isset($order->billing_cep2)) {
                $cep = $order->billing_cep2;
            } else {
                if (isset($order->billing_cep_2)) {
                    $cep = $order->billing_cep_2;
                } else {
                    $cep = $order->billing_postcode;
                }
            }

            $data['end_numero'] = intval($end[1]);
            $data['cpf'] = $order->billing_cpf;
            $data['bairro'] = $order->billing_bairro;
            $data['estado'] = $order->billing_address_2;
            $data['cep'] = $cep;
            $data['cidade'] = $order->billing_city;
            $data['endereco'] = $order->billing_address_1;

            $dados = apply_filters('wcboleto_data', $data, $order);

            $retornoregistro = WC_Boleto::registra_boleto($dados);

            if (intval($retornoregistro->return->situacao) == 0) {
                $dadosboleto = apply_filters('wcboleto_data', $data, $order);

                $dadosboleto['codigo_barras'] = $retornoregistro->return->titulo->cdBarra;
                $dadosboleto['linha_digitavel'] = $retornoregistro->return->titulo->linDig;
                $dadosboleto['nosso_numero'] = $retornoregistro->return->titulo->nossoNumero;

                $strdtencimento = $retornoregistro->return->titulo->dtVencto;

                $datavencimento = substr($strdtencimento, 0, 2) . '/' . substr($strdtencimento, 2, 2) . '/' . substr($strdtencimento, 4, 4);

                $dadosboleto['data_vencimento'] = $datavencimento;
                $dadosboleto['especie_doc'] = $retornoregistro->return->titulo->especie;
                $dadosboleto['especie'] = $retornoregistro->return->titulo->especie;
                $dadosboleto['billing_cpf'] = $order->billing_cpf;

                $dadosboleto['sacado'] = $retornoregistro->return->pagador->nome;

                if (intval($retornoregistro->return->pagador->tpDoc) == 1) {
                    /* tratamento do cpf */
                    $numcpf = intval($retornoregistro->return->pagador->numDoc);
                    $nbr_cpf = str_pad($numcpf, 11, '0', STR_PAD_LEFT);

                    $cpfMask = "%s%s%s.%s%s%s.%s%s%s-%s%s";

                    $monta_cpf = vsprintf($cpfMask, str_split($nbr_cpf));

                    $dadosboleto['num_doc'] = 'CPF: ' . $monta_cpf;
                } else {
                    /* tratamento do cnpj */
                    $numcnpj = intval($retornoregistro->return->pagador->numDoc);
                    $nbr_cnpj = str_pad($numcnpj, 14, '0', STR_PAD_LEFT);

                    $cnpjMask = "%s%s.%s%s%s.%s%s%s/%s%s%s%s-%s%s";

                    $monta_cnpj = vsprintf($cnpjMask, str_split($nbr_cnpj));
                    $dadosboleto['num_doc'] = 'CNPJ: ' . $monta_cnpj;
                }

                $vcep = $retornoregistro->return->pagador->cep;
                $valcep = substr($vcep, 0, 5) . '/' . substr($vcep, 5, 3);

                $dadosboleto['end_cep'] = $valcep;
                $dadosboleto['endereco'] = $retornoregistro->return->pagador->ender;
                $dadosboleto['end_bairro'] = $retornoregistro->return->pagador->bairro;
                $dadosboleto['end_cidade'] = $retornoregistro->return->pagador->cidade;
                $dadosboleto['end_uf'] = $retornoregistro->return->pagador->uf;

                $strdtemissao = $retornoregistro->return->titulo->dtEmissao;

                $dataemissao = substr($strdtemissao, 0, 2) . '/' . substr($strdtemissao, 2, 2) . '/' . substr($strdtemissao, 4, 4);

                $dadosboleto['data_processamento'] = $dataemissao;

                // Include bank templates.
                include WC_Boleto::get_plugin_path() . 'includes/banks/' . $bank . '/functions.php';
                include WC_Boleto::get_plugin_path() . 'includes/banks/' . $bank . '/layout.php';
            } else {
                echo 'Situação: ' . $retornoregistro->return->descricaoErro;
            }

            exit;
        }
    }
}

// If an error occurred is redirected to the homepage.
wp_redirect(home_url());
exit;
