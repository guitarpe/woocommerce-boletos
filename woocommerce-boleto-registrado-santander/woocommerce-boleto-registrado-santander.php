<?php
/**
 * Plugin Name: WooCommerce Boleto Registrado Santander
 * Plugin URI:
 * Description: WooCommerce Boleto is a brazilian payment gateway for WooCommerce
 * Author: Flavio Santos Sousa
 * Author URI: http://foottsdev.com.br
 * Version: 1.0
 * License: GPLv2 or later
 * Text Domain: woocommerce-boleto-registrado-santander
 * Domain Path: /languages/
 */
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WC_Boleto')) :

    /**
     * WooCommerce Boleto main class.
     */
    class WC_Boleto
    {

        /**
         * Plugin version.
         *
         * @var string
         */
        const VERSION = '1.0';

        /**
         * Instance of this class.
         *
         * @var object
         */
        protected static $instance = null;
        private static $registro;

        /**
         * Initialize the plugin actions.
         */
        private function __construct()
        {
            // Load plugin text domain
            add_action('init', array($this, 'load_plugin_textdomain'));

            // Checks with WooCommerce is installed.
            if (class_exists('WC_Payment_Gateway')) {
                // Public includes.
                $this->includes();

                // Admin includes.
                if (is_admin()) {
                    $this->admin_includes();
                }

                add_filter('woocommerce_payment_gateways', array($this, 'add_gateway'));
                add_action('init', array(__CLASS__, 'add_boleto_endpoint'));
                add_filter('template_include', array($this, 'boleto_template'), 9999);
                add_action('woocommerce_view_order', array($this, 'pending_payment_message'));
                add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'plugin_action_links'));
            } else {
                add_action('admin_notices', array($this, 'woocommerce_missing_notice'));
            }

            self::$registro = new RegistraBoleto();
        }

        /**
         * Return an instance of this class.
         *
         * @return object A single instance of this class.
         */
        public static function get_instance()
        {
            // If the single instance hasn't been set, set it now.
            if (null == self::$instance) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        /**
         * Get plugin path.
         *
         * @return string
         */
        public static function get_plugin_path()
        {
            return plugin_dir_path(__FILE__);
        }

        /**
         * Load the plugin text domain for translation.
         */
        public function load_plugin_textdomain()
        {
            $locale = apply_filters('plugin_locale', get_locale(), 'woocommerce-boleto-registrado-santander');

            load_textdomain('woocommerce-boleto-registrado-santander', trailingslashit(WP_LANG_DIR) . 'woocommerce-boleto-registrado-santander/woocommerce-boleto-registrado-santander-' . $locale . '.mo');
            load_plugin_textdomain('woocommerce-boleto-registrado-santander', false, dirname(plugin_basename(__FILE__)) . '/languages/');
        }

        /**
         * Includes.
         */
        private function includes()
        {
            include_once 'includes/wc-boleto-functions.php';
            include_once 'includes/class-wc-boleto-gateway.php';
            include_once 'includes/RegistraBoleto.php';
        }

        /**
         * Includes.
         */
        private function admin_includes()
        {
            require_once 'includes/class-wc-boleto-admin.php';
        }

        /**
         * Add the gateway to WooCommerce.
         *
         * @param  array $methods WooCommerce payment methods.
         *
         * @return array Payment methods with Boleto.
         */
        public function add_gateway($methods)
        {
            $methods[] = 'WC_Boleto_Gateway';

            return $methods;
        }

        /**
         * Created the boleto endpoint.
         */
        public static function add_boleto_endpoint()
        {
            add_rewrite_endpoint('boleto', EP_PERMALINK | EP_ROOT);
        }

        /**
         * Plugin activate method.
         */
        public static function activate()
        {
            self::add_boleto_endpoint();

            flush_rewrite_rules();
        }

        /**
         * Plugin deactivate method.
         */
        public static function deactivate()
        {
            flush_rewrite_rules();
        }

        /**
         * Add custom template page.
         *
         * @param  string $template
         *
         * @return string
         */
        public function boleto_template($template)
        {
            global $wp_query;

            if (isset($wp_query->query_vars['boleto'])) {
                return self::get_plugin_path() . 'includes/views/html-boleto.php';
            }

            return $template;
        }

        /**
         * Gets the boleto URL.
         *
         * @param  string $code Boleto code.
         *
         * @return string       Boleto URL.
         */
        public static function get_boleto_url($code)
        {
            $home = home_url('/');

            if (get_option('permalink_structure')) {
                $url = trailingslashit($home) . 'boleto/' . $code;
            } else {
                $url = add_query_arg(array('boleto' => $code), $home);
            }

            return apply_filters('woocommerce_boleto_url', $url, $code, $home);
        }

        /**
         * Display pending payment message in order details.
         *
         * @param  int $order_id Order id.
         *
         * @return string        Message HTML.
         */
        public function pending_payment_message($order_id)
        {
            $order = new WC_Order($order_id);

            if ('on-hold' === $order->status && 'boleto' == $order->payment_method) {
                $html = '<div class="woocommerce-info">';
                $html .= sprintf('<a class="button" href="%s" target="_blank" style="display: block !important; visibility: visible !important;">%s</a>', esc_url(wc_boleto_get_boleto_url($order->order_key)), __('Pay the Ticket &rarr;', 'woocommerce-boleto-registrado-santander'));

                $message = sprintf(__('%sAttention!%s Not registered the payment the docket for this product yet.', 'woocommerce-boleto-registrado-santander'), '<strong>', '</strong>') . '<br />';
                $message .= __('Please click the following button and pay the Ticket in your Internet Banking.', 'woocommerce-boleto-registrado-santander') . '<br />';
                $message .= __('If you prefer, print and pay at any bank branch or lottery retailer.', 'woocommerce-boleto-registrado-santander') . '<br />';
                $message .= __('Ignore this message if the payment has already been made​​.', 'woocommerce-boleto-registrado-santander') . '<br />';

                $html .= apply_filters('wcboleto_pending_payment_message', $message, $order);

                $html .= '</div>';

                echo $html;
            }
        }

        /**
         * Action links.
         *
         * @param  array $links
         *
         * @return array
         */
        public function plugin_action_links($links)
        {
            $plugin_links = array();

            if (defined('WC_VERSION') && version_compare(WC_VERSION, '2.1', '>=')) {
                $settings_url = admin_url('admin.php?page=wc-settings&tab=checkout&section=wc_boleto_gateway');
            } else {
                $settings_url = admin_url('admin.php?page=woocommerce_settings&tab=payment_gateways&section=WC_Boleto_Gateway');
            }

            $plugin_links[] = '<a href="' . esc_url($settings_url) . '">' . __('Settings', 'woocommerce-boleto-registrado-santander') . '</a>';

            return array_merge($plugin_links, $links);
        }

        /**
         * WooCommerce fallback notice.
         *
         * @return string
         */
        public function woocommerce_missing_notice()
        {
            include_once 'includes/views/html-notice-woocommerce-missing.php';
        }

        public function registra_boleto($dadosboleto)
        {
            $registrar = new RegistraBoleto();

            setlocale(LC_CTYPE, 'pt_BR');

            //$param = [];
            $estados = ['Acre', 'Alagoas', 'Amap&aacute;', 'Amazonas', 'Bahia', 'Cear&aacute;', 'Distrito_Federal', 'Esp&iacute;rito_Santo', 'Goi&aacute;s', 'Maranh&atilde;o', 'Mato_Grosso', 'Mato_Grosso_do_Sul', 'Minas_Gerais', 'Par&aacute;', 'Para&iacute;ba', 'Paran&aacute;', 'Pernambuco', 'Piau&iacute;', 'Rio_de_Janeiro', 'Rio_Grande_do_Norte', 'Rio_Grande_do_Sul', 'Rond&ocirc;nia', 'Roraima', 'Santa_Catarina', 'S&atilde;o_Paulo', 'Sergipe', 'Tocantins'];
            $siglas = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];

            $nnumcep = $dadosboleto["cep"];
            $estado = $dadosboleto["estado"];
            $estadouf = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $estado));
            $cidade = substr($dadosboleto["cidade"], 0, 20);
            $ncpfcnpj = preg_replace("/[^0-9]/", "", $dadosboleto["cpf"]);
            $uf = preg_replace("/ /", "_", $estadouf);
            $key = array_search($uf, $estados);

            $mensagem = $dadosboleto["demonstrativo1"];
            $datalimite = date('dmY');

            $dados['PAGADOR.TP-DOC'] = count($ncpfcnpj) >= 13 ? '02' : '01';
            $dados['PAGADOR.CEP'] = !empty($nnumcep) ? preg_replace("/[^A-Za-z0-9]/", "", $nnumcep) : '';
            $dados['PAGADOR.NOME'] = substr($dadosboleto["sacado"], 0, 40);
            $dados['PAGADOR.NUMERO'] = $dadosboleto["end_numero"];
            $dados['PAGADOR.ENDER'] = substr($dadosboleto["endereco"], 0, 40);
            $dados['PAGADOR.BAIRRO'] = substr($dadosboleto["bairro"], 0, 30);
            $dados['PAGADOR.CIDADE'] = !empty($cidade) ? preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $cidade)) : '';
            $dados['PAGADOR.UF'] = $siglas[$key];
            $dados['PAGADOR.NUM-DOC'] = str_pad($ncpfcnpj, 14, '0', STR_PAD_LEFT);
            $dados['TITULO.ESPECIE'] = !empty($dadosboleto["especie_doc"]) ? $dadosboleto["especie_doc"] : 04;
            $dados['TITULO.NOSSO-NUMERO'] = str_pad($dadosboleto["numero_documento"], 11, '0', STR_PAD_LEFT);
            $dados['TITULO.SEU-NUMERO'] = str_pad($dadosboleto["numero_documento"], 11, '0', STR_PAD_LEFT);
            $dados['TITULO.DT-EMISSAO'] = !empty($dadosboleto["data_processamento"]) ? str_replace("/", "", $dadosboleto["data_processamento"]) : '';
            $dados['TITULO.DT-VENCTO'] = !empty($dadosboleto["data_vencimento"]) ? str_replace("/", "", $dadosboleto["data_vencimento"]) : '';
            $dados['TITULO.VL-NOMINAL'] = !empty($dadosboleto["valor_boleto"]) ? preg_replace("/[^0-9]/", "", $dadosboleto["valor_boleto"]) : '';
            $dados['TITULO.PC-MULTA'] = $dadosboleto["titulo_pc_multa"];
            $dados['TITULO.QT-DIAS-MULTA'] = $dadosboleto["titulo_qt_dias_multa"];
            $dados['TITULO.PC-JURO'] = $dadosboleto["titulo_pc_juro"];
            $dados['TITULO.TP-DESC'] = $dadosboleto["titulo_tp_desc"];
            $dados['TITULO.VL-DESC'] = $dadosboleto["titulo_vl_desc"];
            $dados['TITULO.DT-LIMI-DESC'] = !empty($dadosboleto["titulo_dt_limi_desc"]) ? str_replace("/", "", $dadosboleto["titulo_dt_limi_desc"]) : $datalimite;
            $dados['TITULO.VL-ABATIMENTO'] = $dadosboleto["titulo_vl_abatimento"];
            $dados['TITULO.TP-PROTESTO'] = $dadosboleto["titulo_tp_protesto"];
            $dados['TITULO.QT-DIAS-PROTESTO'] = $dadosboleto["titulo_qt_dias_protesto"];
            $dados['TITULO.QT-DIAS-BAIXA'] = $dadosboleto["titulo_qt_dias_baixa"];
            $dados['TITULO.TP-PAGAMENTO'] = $dadosboleto["titulo_tp_pagamento"];
            $dados['TITULO.QT-PARCIAIS'] = $dadosboleto["titulo_qt_parciais"];
            $dados['TITULO.TP-VALOR'] = $dadosboleto["titulo_tp_valor"];
            $dados['TITULO.VL-PERC-MINIMO'] = $dadosboleto["titulo_vl_perc_minimo"];
            $dados['TITULO.VL-PERC-MAXIMO'] = $dadosboleto["titulo_vl_perc_maximo"];
            $dados['MENSAGEM'] = substr($mensagem, 0, 100);

            $pastacert = WP_PLUGIN_DIR . "/woocommerce-boleto-registrado-santander/cert/";

            return $registrar->registrarBoleto($dados, $pastacert);
        }
    }

    /**
     * Plugin activation and deactivation methods.
     */
    register_activation_hook(__FILE__, array('WC_Boleto', 'activate'));
    register_deactivation_hook(__FILE__, array('WC_Boleto', 'deactivate'));

    /**
     * Initialize the plugin.
     */
    add_action('plugins_loaded', array('WC_Boleto', 'get_instance'));

endif;
