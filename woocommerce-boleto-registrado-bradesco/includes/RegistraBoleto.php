<?php  
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Classe de conex�o com o WS do bradesco para registro autom�tico de boleto
 * @author Fl�vio Santos Sousa
 */

class RegistraBoleto {
	
	protected $certificado;
	protected $senha;
	protected $urlwshomolog;
	protected $urlwsregistro;
	
	function __construct() {
		$this->certificado 	= '11974398_out.pfx';//'sotelab.pfx';
		$this->senha 		= '669080666';//'wlte_59J7J';
		$this->urlwshomolog 		= 'https://cobranca.bradesconetempresa.b.br/ibpjregistrotitulows/registrotitulohomologacao';
		$this->urlwsregistro 		= 'https://cobranca.bradesconetempresa.b.br/ibpjregistrotitulows/registrotitulo';
	}
	
	function homologacao($mensagem, $certfolder){
		try{
			file_put_contents($certfolder.'file_msg', $mensagem);
	
			$certificado_pfx = file_get_contents($certfolder.$this->certificado);
	
			if (!openssl_pkcs12_read($certificado_pfx, $result, $this->senha)) {
				throw new Exception('N�o foi poss�vel ler o certificado .pfx');
			}
	
			$certificado_key = openssl_x509_read($result['cert']);
	
			$private_key = openssl_pkey_get_private($result['pkey'], $this->senha);
	
			openssl_pkcs7_sign(
			$certfolder.'file_msg', $certfolder.'signature', $certificado_key, $private_key, [], PKCS7_BINARY | PKCS7_TEXT
			);
	
			$signature = file_get_contents($certfolder.'signature');
	
			$parts = preg_split("#\n\s*\n#Uis", $signature);
	
			$mensagem_assinada_base64 = $parts[1];
	
			$ch = curl_init($this->urlwshomolog);
	
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $mensagem_assinada_base64);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
			$retorno = curl_exec($ch);
	
			if (curl_errno($ch)) {
				$info = curl_getinfo($ch);
				throw new Exception('N�o foi poss�vel homologar o boleto. ' . 'Erro:' . curl_errno($ch) . '.<br>' . $info);
			}
	
			$doc = new DOMDocument();
			$doc->loadXML($retorno);
	
			$retorno = $doc->getElementsByTagName('return')->item(0)->nodeValue;
			$retorno = preg_replace('/, }/i', '}', $retorno);
			$retorno = json_decode($retorno);
	
			if (!empty($retorno->cdErro)) {
				throw new Exception('Erro: '.$retorno->cdErro.' - N�o foi poss�vel homologar o boleto - ' . $retorno->msgErro);
			}
				
			return $retorno;
				
		}catch(Exception $e){
			return $retorno;
		}
	}
	
	function registro($certfolder){
		try{
			
			$signature = file_get_contents($certfolder.'signature');
	
			$parts = preg_split("#\n\s*\n#Uis", $signature);
	
			$mensagem_assinada_base64 = $parts[1];
	
			$ch = curl_init($this->urlwsregistro);
	
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $mensagem_assinada_base64);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
			$retorno = curl_exec($ch);
	
			if (curl_errno($ch)) {
				$info = curl_getinfo($ch);
				throw new Exception('N�o foi poss�vel registrar o boleto. ' . 'Erro:' . curl_errno($ch) . '.<br>' . $info);
			}
	
			$doc = new DOMDocument();
			$doc->loadXML($retorno);
	
			$retorno = $doc->getElementsByTagName('return')->item(0)->nodeValue;
			$retorno = preg_replace('/, }/i', '}', $retorno);
			$retorno = json_decode($retorno);
	
			if (!empty($retorno->cdErro)) {
				throw new Exception('Erro: '.$retorno->cdErro.' - N�o foi poss�vel registrar o boleto - ' . $retorno->msgErro);
			}
	
			return $retorno;
	
		}catch(Exception $e){
			return $retorno;
		}
	}
	
	function conectarWS($mensagem, $certfolder){
		try{
			$retorno = null;
			
			$homologacao = $this->homologacao($mensagem, $certfolder);
			
			if (empty($homologacao->cdErro)) {
				$retorno = $this->registro($certfolder);
			}else{
				$retorno = $homologacao;
			}
			
			return $retorno;
			
		}catch(Exception $e){
			return $retorno;
		}
	}
}

?>